push:
	make commit
	git push

push-amend:
	make amend
	git push
	
commit:
	git pull
	git add .
	git commit

amend:
	git pull
	git add .
	git commit --amend --no-edit

u: 
	git pull